@extends('layouts.app')
@section('content')

  <form class="form form-horizontal" action="{{route('user.update',['id'=>($user->id)])}}" method="POST">
    @csrf
    <div class="form-body">
      <div class="form-group row">
        <label class="col-sm-3 form-control-label" for="inputName1">Name</label>
        <div class="col-sm-9">
          <div class="position-relative has-icon-left">
            <input class="form-control" type="text" name="name" id="inputName1" value="{{$user->name}}" placeholder="Name">
            <div class="form-control-position pl-1"><i class="fa fa-user"></i></div>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-3 form-control-label" for="inputEmail1">Email</label>
        <div class="col-sm-9">
          <div class="position-relative has-icon-left">
            <input class="form-control" type="email" name="email" id="inputEmail1" value="{{$user->email}}" placeholder="Email">
            <div class="form-control-position pl-1"><i class="fa fa-envelope"></i></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 mb-1">
          <button class="btn btn-info float-right" type="submit"><i class="fa fa-paper-plane"></i> Update          </button>
          <a class="btn btn-info float-left" href="{{route('dashboard.index')}}"><i class="fa fa-arrow-circle-left"></i> Back          </a>
        </div>
      </div>
    </div>
  </form>

@endsection
