@extends('layouts.app')
@section('content')

      <div class="row match-height">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title" id="basic-layout-tooltip">Assign Projects</h4>
              <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                  <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                  <li><a data-action="close"><i class="ft-x"></i></a></li>
                </ul>
              </div>
            </div>

            <div class="card-content collapse show">
              <div class="card-body">

                <form class="form" action="{{route('assignpro.index')}}" method="POST">
                  @csrf
                  <div class="form-body">

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="issueinput1">User Name</label>
                          <select id="issueinput1" name="uid" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
                            <option disabled selected>Select User name</option>
                            @foreach($user as $value)
                              <option value="{{$value->id}}">{{$value->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="issueinput2">Project Name</label>
                          <select id="issueinput2" name="pid" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Priority">
                            <option disabled selected>Select Project Name</option>
                            @foreach ($project as $value1)
                              <option value="{{$value1->pid}}">{{$value1->pname}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="issueinput3">Project Start Date</label>
                          <input type="date" id="issueinput3" class="form-control" name="pstd" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Date Opened">
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="issueinput4">Project End Date</label>
                          <input type="date" id="issueinput4" class="form-control" name="pend" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Date Fixed">
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="issueinput5">Project Schedule Date</label>
                          <input type="date" id="issueinput5" class="form-control" name="pscd" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Date Fixed">
                        </div>
                      </div>
                    </div>

                  </div>

                  <div class="form-actions">
                    <a href="{{route('dashboard.index')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Back</a>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Assign</button>
                  </div>
                </form>

              </div>
            </div>

          </div>
        </div>

      </div>

@endsection
