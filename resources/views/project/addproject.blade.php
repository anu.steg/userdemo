@extends('layouts.app')
@section('content')

  <form class="form" action="{{route('store.index')}}" enctype="multipart/form-data" method="post">
    <div class="form-actions top">
      <a href="{{route('project.index')}}" class="btn btn-warning mr-1"><i class="ft-x"></i> Back</a>
      <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Save</button>
    </div>
    @csrf
    <div class="form-body">
      <h4 class="form-section"><i class="ft-user"></i> Project Info</h4>
      <div class="row">
        <div class="form-group col-md-12 mb-2">
          <label for="projectinput1">Project Name</label>
          <input type="text" id="projectinput1" class="form-control" placeholder="Project Name" name="pname">
        </div>
        <div class="form-group col-md-12 mb-2">
          <label for="projectinput2">Project Description</label>
          <textarea type="text" id="projectinput2" class="form-control" placeholder="Project Description" name="pdescription"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="form-group col-12 mb-2">
          <label>Select File</label>
          <label id="projectinput8" class="file center-block">
            <input type="file" id="file" name="pdocument">
            <span class="file-custom"></span>
          </label>
        </div>
      </div>
    </div>
  </form>

@endsection
