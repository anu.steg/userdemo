<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class LoginController extends Controller
{
  public function index(){
    try {
      return view('loginpage');
    } catch (\Exception $e) {
      return redirect()->back()->withErrors($e->getMessage());
    }
  }

  public function checklogin(Request $request){
    try {
      if (User::where(['email' => $request->email])->first()) {
          if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            if ( Auth::user()->role == 1 ) {
              return redirect()->route('dashboard.index')->with('success','Login Successfully');
            }else {
              return redirect()->route('userdashboard.index')->with('success','Login Successfully');
            }
          }else {
              return redirect()->back()->with('error' , 'Invalid Password!');
          }
      }else {
          return redirect()->back()->with('error' , 'Invalid Email.');
      }
    } catch (\Exception $e) {
      return redirect()->back()->withErrors($e->getMessage());
    }
  }

  public function logout(){
    try {

      Auth::logout();
      return redirect()->route('login.index')->with('success','Successfully');

    } catch (\Exception $e) {

      return redirect()->back()->withErrors($e->getMessage());

    }
  }
}
