<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Project;
use Validater;
use File;
use App\Assign;

class ProjectController extends Controller
{
    // public function index()
    // {
    //     $data = Project::get();
    //     return view('')->with('data',$data);
    // }
    public function add()
    {
      return view('project.addproject');
    }

    public function store(Request $request) {

      $this->validate($request,[

        'pname' => 'required',
        'pdescription' => 'required'
      ]);

      if($request->hasFile('pdocument'))
      {
      // Get File Name with extension
      $fileNameWithExt = $request->file('pdocument')->getClientOriginalName();
      // File Name
      $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
      // Extension
      $ext = $request->file('pdocument')->getClientOriginalExtension();
      $fileNameToStore = $fileName.'_'.time().'.'.$ext;
      $path = $request->file('pdocument')->storeAs('public/upload',$fileNameToStore);
      }
      else
      {
      $fileNameToStore ="No Image.jpg";
      }

      $store = new Project;
      $store->pname = $request->pname;
      $store->pdescription = $request->pdescription;
      $store->pdocument = $fileNameToStore;
      $store->save();

        return redirect()->route('project.index')->with('success', 'Project add successfully');
    }

    public function edit($pid) {

      $project = Project::find($pid);
      return view('project.update')->with('project', $project);
    }

    public function update(Request $request,$pid ) {

      $this->validate($request,[

        'pname' => 'required',
        'pdescription' => 'required'
      ]);

      if($request->hasFile('pdocument'))
      {
      // Get File Name with extension
      $fileNameWithExt = $request->file('pdocument')->getClientOriginalName();
      // File Name
      $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
      // Extension
      $ext = $request->file('pdocument')->getClientOriginalExtension();
      $fileNameToStore = $fileName.'_'.time().'.'.$ext;
      $path = $request->file('pdocument')->storeAs('public/upload',$fileNameToStore);
      }
      else
      {
      $fileNameToStore ="No Image.jpg";
      }

      $update = Project::find($pid);
      $update->pname = $request->pname;
      $update->pdescription = $request->pdescription;
      $update->pdocument = $fileNameToStore;
      $update->save();

        return redirect()->route('project.index')->with('success', 'Project update successfully');
    }

    public function delete($pid) {

      $delete = Project::find($pid);
      Storage::delete('public/upload/' . $delete->pdocument);
      $delete->delete();

        return redirect()->route('project.index')->with('success', 'Project Delete successfully');
    }

    public function assignproject(Request $request)
     {

      $this->validate($request,[

        'uid' => 'required',
        'pid' => 'required',
        'pstd' => 'required',
        'pend' => 'required',
        'pscd' => 'required'

      ]);

      $assign = new Assign;

      $assign->uid = $request->uid;
      $assign->pid = $request->pid;
      $assign->pstartdate = $request->pstd;
      $assign->penddate = $request->pend;
      $assign->pschedule = $request->pscd;
      $assign->save();

      return redirect()->route('dashboard.index')->with('success', 'Assign Project successfully');
    }

}
